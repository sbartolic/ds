import numpy as np
from mpi4py import MPI
import math
import time



comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()

n_iterations = 1000


def compute_e(n):

    sum = 0
    for i in range(0, n):
        h = 1
        f_n = math.factorial(i)
        sum += h/f_n
    return sum

start = time.time()
e_part=np.empty(1, dtype=np.float64)
e_part[0] = compute_e(n_iterations)
end = time.time()
print("Procesu",rank,"za izra�unavanje trebalo je ", end - start, "sek")

if rank == 0:

    e_br = np.empty(1, dtype = np.float64)

else:

    e_br = None

comm.Reduce(e_part, e_br, op=MPI.SUM, root=0)
print(e_part)

if rank == 0:
    error = abs(e_br/size - np.math.e)

    print("Vrijednost broja e/baze prirodnog logaritma je pribli�no %.16f" %(e_br/size))
    print("Gre�ka pri ra�unanju iznosi pribli�no %.16f" %(error))
